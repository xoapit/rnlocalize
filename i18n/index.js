import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';

import {I18nManager} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const translationGetters = {
  en: () => require('./en.json'),
  nl: () => require('./nl.json'),
};

const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

const setI18nConfig = async () => {
  // fallback if no available language fits
  const fallback = {languageTag: 'en', isRTL: false};

  const {languageTag, isRTL} =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;

  I18nManager.forceRTL(isRTL);

  i18n.translations = {
    en: translationGetters.en(),
    nl: translationGetters.nl(),
  };

  const languageCode = await AsyncStorage.getItem('LANGUAGE');
  i18n.locale = languageCode || languageTag;

  translate.cache.clear();
};

const changeLocale = (locale) => {
  i18n.locale = locale;
};

export const I18n = {
  changeLocale,
  setI18nConfig,
  translate,
};
