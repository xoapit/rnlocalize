import React from 'react';
import * as RNLocalize from 'react-native-localize';
import RNRestart from 'react-native-restart';

import {
  Platform,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {I18n} from '../i18n';

export class SyncExample extends React.Component {
  constructor(props) {
    super(props);
    I18n.setI18nConfig().then(() => {
      this.forceUpdate();
    });
  }

  componentDidMount() {
    RNLocalize.addEventListener('change', this.handleLocalizationChange);
  }

  componentWillUnmount() {
    RNLocalize.removeEventListener('change', this.handleLocalizationChange);
  }

  handleLocalizationChange = () => {
    I18n.setI18nConfig();
    this.forceUpdate();
  };

  onChangeLocale = async (languageCode) => {
    I18n.changeLocale(languageCode);
    await AsyncStorage.setItem('LANGUAGE', languageCode);
    RNRestart.Restart();
  };

  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <ScrollView contentContainerStyle={styles.container}>
          <Button
            onPress={() => this.onChangeLocale('en')}
            title={'Change to EN'}
          />
          <Button
            onPress={() => this.onChangeLocale('nl')}
            title={'Change to NL'}
          />
          <Button
            onPress={() => {
              alert(I18n.translate('hello'));
            }}
            title={'Ok'}
          />
          <Line name="Translation example" value={I18n.translate('hello')} />
          <Line
            name="RNLocalize.getLocales()"
            value={RNLocalize.getLocales()}
          />
          <Line
            name="RNLocalize.getCurrencies()"
            value={RNLocalize.getCurrencies()}
          />
          <Line
            name="RNLocalize.getCountry()"
            value={RNLocalize.getCountry()}
          />
          <Line
            name="RNLocalize.getCalendar()"
            value={RNLocalize.getCalendar()}
          />
          <Line
            name="RNLocalize.getNumberFormatSettings()"
            value={RNLocalize.getNumberFormatSettings()}
          />
          <Line
            name="RNLocalize.getTemperatureUnit()"
            value={RNLocalize.getTemperatureUnit()}
          />
          <Line
            name="RNLocalize.getTimeZone()"
            value={RNLocalize.getTimeZone()}
          />
          <Line
            name="RNLocalize.uses24HourClock()"
            value={RNLocalize.uses24HourClock()}
          />
          <Line
            name="RNLocalize.usesMetricSystem()"
            value={RNLocalize.usesMetricSystem()}
          />

          {Platform.OS === 'android' && (
            <>
              <Line
                name="RNLocalize.usesAutoDateAndTime()"
                value={RNLocalize.usesAutoDateAndTime()}
              />
              <Line
                name="RNLocalize.usesAutoTimeZone()"
                value={RNLocalize.usesAutoTimeZone()}
              />
            </>
          )}

          <Line
            name="RNLocalize.findBestAvailableLanguage(['en-US', 'en', 'fr'])"
            value={RNLocalize.findBestAvailableLanguage(['en-US', 'en', 'fr'])}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const Line = (props) => (
  <View style={styles.block}>
    <Text style={styles.name}>{props.name}</Text>
    <Text style={styles.value}>{JSON.stringify(props.value, null, 2)}</Text>
  </View>
);

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: 'white',
    flex: 1,
  },
  container: {
    padding: 16,
    alignItems: 'flex-start',
  },
  block: {
    marginBottom: 16,
    alignItems: 'flex-start',
  },
  name: {
    textDecorationLine: 'underline',
    fontWeight: '500',
    marginBottom: 8,
  },
  value: {
    textAlign: 'left',
  },
});
